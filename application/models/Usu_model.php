<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Usu_model extends CI_Model{

    //função de inserir um novo usuario
    public function usuario_add($dados_usuario){
		//insert_id pega o ultimo id
      $this->db->insert('cadastros',$dados_usuario);
  }

  public function get($dados){
    $this->db->where("nome", $dados["nome"]);
    $this->db->where("senha", $dados["senha"]);
    $usuario = $this->db->get("cadastros")->result();
    return $usuario[0];
}
public function buscar_usuario($id){
 $this->db->from("cadastros");
 $this->db->where('id='. $id);
 $query=$this->db->get();
 return $query->result();
}

public function modifica($dados){
   
    $this->db->where("id", $dados["id"]);
    return $this->db->update('cadastros',$dados); 

}

public function validar_cnpj($cnpj){
 $this->db->from("cadastros");
 $this->db->where('cnpj='. $cnpj);
 $query=$this->db->get();
 return $query->result();
}

public function apagar($id){
   
    $this->db->where("id", $id);
    return $this->db->delete('cadastros'); 

}




}


?>