<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<meta charset="utf-8">
	<title>cadastrar</title>
</head>
<body>
	

	<form action="<?php echo base_url('salvar');?>"  method="post"  enctype="multipart/form-data">
		<div class="form-group">
			<label for="exampleInputEmail1">nome</label>
			<input name="nome" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="nome" required>

		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">senha</label>
			<input name="senha" type="password" class="form-control" id="exampleInputPassword1" placeholder="senha" required>
		</div>
		<div class="form-group">
			<label>cnpj</label>
			<input name="cnpj" minlength="14"  maxlength="14" type="number" class="form-control"  placeholder="cnpj"  required>
		</div>
		<div class="form-group">
			<label for="exampleFormControlFile1">escolha um logo</label>
			<input name="foto" type="file" class="form-control-file" id="exampleFormControlFile1">
		</div>

		<button type="submit" class="btn btn-primary">cadastrar</button>
	</form>

</div>
</body>
</html>