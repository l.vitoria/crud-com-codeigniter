<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<meta charset="utf-8">
	<title>login</title>
	
</head>
<body>

	<form action="<?php echo base_url('login');?>"  method="post">
		<div class="form-group">
			<label for="exampleInputEmail1">nome</label>
			<input name="nome" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" required>
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">senha</label>
			<input name="senha" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" required>
		</div>
		<button type="submit" class="btn btn-primary">logar</button>
	</form>
	<br>
	<a href="<?php echo base_url('cadastrar'); ?>" type="submit" class="btn btn-primary">cadastra-se</a>
</div>
</body>
</html>