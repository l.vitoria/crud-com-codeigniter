<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

	public function __construct(){
		/*Carrega o Model usuario
		tudo que eu colocar aqui carregar em todas as funções*/
		parent:: __construct();
		$this->load->model('Usu_model','usuario');
		$this->load->library('form_validation');
	}



	public function index()
	{
		$this->load->view('cadastro');
	}

	public function danger()
	{
		$this->load->view('alerts/danger');
	}


	public function salvar()
	{	

		$filename = $_FILES['foto']['name'];

		$curriculo    = $_FILES['foto']['name'];
		/*var_dump($_FILES);
		die;*/
		$configuracao = array(
			'upload_path'   => './imagem/',
			'allowed_types' => 'jpg|png',
			'file_name'     => $filename,
			'max_size'      => '1500'
		);   

		$this->load->library('upload');
		$this->upload->initialize($configuracao);

		if ($this->upload->do_upload('foto')){
			echo 'Arquivo salvo com sucesso.';
		}
		else{
			echo $this->upload->display_errors();
		}

		$cnpj=$this->input->post('cnpj');


		$testar_cnpj=$this->usuario->validar_cnpj($cnpj);



		if ($testar_cnpj) {
			redirect('danger');

		}


		$dados_usuario = array(
			'nome' => $this->input->post('nome'),
			'cnpj'=> $cnpj,
			'senha'=> $this->input->post('senha'),
			'foto' => $configuracao['file_name']);


		
		/*var_dump($dados_usuario);
		die;*/
		



		$this->usuario->usuario_add($dados_usuario);

		redirect('Login');
		
		
	}



	public function editar()
	{		
		$id=$this->session->userdata('usuario'); 
		$dados['listar']=$this->usuario->buscar_usuario($id);
		$this->load->view('editar',$dados);
	}


	public function salvar_edicao()
	{	

		$filename = $_FILES['foto']['name'];

		$curriculo    = $_FILES['foto']['name'];
		/*var_dump($_FILES);
		die;*/
		$configuracao = array(
			'upload_path'   => './imagem/',
			'allowed_types' => 'jpg|png',
			'file_name'     => $filename,
			'max_size'      => '1500'
		);   

		$this->load->library('upload');
		$this->upload->initialize($configuracao);

		if ($this->upload->do_upload('foto'))
			echo 'Arquivo salvo com sucesso.';
		else
			echo $this->upload->display_errors();


			$foto_antiga = $this->input->post('foto_antiga');


		if(empty($configuracao['file_name'])){

			$configuracao['file_name']= $foto_antiga;
		}

	
		$dados_usuario = array(
			'nome' => $this->input->post('nome'),
			'cnpj'=>  $this->input->post('cnpj'),
			'senha'=> $this->input->post('senha'),
			'id'=> $this->input->post('id'),
			'foto' => $configuracao['file_name']);


		
		/*var_dump($dados_usuario);
		die;*/
		



		$this->usuario->modifica($dados_usuario);
		//$id=$this->paciente->usuario_add($dados_usuario);

		redirect('perfil');
	}



	public function carregar_perfil(){
		$id=$this->session->userdata('usuario'); 
		$dados['listar']=$this->usuario->buscar_usuario($id);
		$this->load->view('perfil',$dados);
	}

	public function excluir(){
		$id=$this->session->userdata('usuario'); 
		$this->usuario->apagar($id);
		redirect('deslogar');
	}


	public function deslogar()
		{
			$this->session->sess_destroy();
			redirect('Login');

		}

}
