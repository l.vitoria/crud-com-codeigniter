<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		/*Carrega o Model usuario
		tudo que eu colocar aqui carregar em todas as funções*/
		parent:: __construct();
		$this->load->model('Usu_model','usuario');
	}

	public function index()
	{
		$this->load->view('Tela_login');
	}


	//pra fazer o login
	public function logar(){

		$dados = array(
			'nome'=> $this->input->post('nome'),
			'senha'=> $this->input->post('senha'));

		$usuario=$this->usuario->get($dados);

		
		if(!empty($usuario)){
			$this->session->set_userdata("usuario", $usuario->id);
			redirect('perfil');
		} 
	}
}
